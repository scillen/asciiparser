/**
 * Created by ellen on 10/04/16.
 */

"use strict";

const IO = require('./src/IO');
const Parser = require('./src/parser');

// Current config params
const config = {
    invoiceNumberLength: 9
};

main();

function main() {

    // Get the in/out file names
    var inputFile = process.argv[2];
    var outputFile = process.argv[3];

    // Init the modules
    var io = new IO(inputFile, outputFile);
    var parser = new Parser(config);

    // Represents the current line index in the ascii number that consists of 3 lines.
    var index = 0;

    // Read the input file line by line
    io.readLine((err, line) => {
        if(err) {
            console.error(err);
        }
        else {
            // Parse the line to a number
            parser.parseRow(line, index, (err, number) => {
                if(err) {
                    console.error(err);
                }
                else {
                    // Write the number to a file
                    io.writeLine(number);

                }
            });
            index = (index < 3)? index + 1: 0;
        }
    });
}


