# ASCII 7-segment Numbers Parser #

## README ##
Given an input text file of 7-segment numbers, this program outputs a text file with the parsed numbers.

### Version ###
1.0.0

### How do I get set up? ###
1. Install NodeJS v4 LTS
2. Install npm
3. Clone the project

### How to run ###
1. cd to .../invoiceparser/
2. npm install
3. node app.js <INPUT_FILE_PATH> <OUTPUT_FILE_PATH>

### EXAMPLE ###
node app.js files/input.txt files/output.txt

### HOW TO RUN TESTS ###
1. npm install -g mocha
2. mocha

### Who do I talk to? ###
Ellen Rapaport