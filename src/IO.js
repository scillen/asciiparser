/**
 * Created by ellen on 10/04/16.
 */

"use strict";

const readline = require('readline');
const fs = require('fs');

class IO {
    constructor(inputFilePath, outputFilePath){

        this.inputFile = inputFilePath;
        this.outputFile = outputFilePath;

        console.log("Starting to read input from "+this.inputFile+ " and write to "+this.outputFile);

        // Init reader interface
        this.lineReader = readline.createInterface({
            input: fs.createReadStream(this.inputFile),
            terminal: true
        });

        // Remove previous content of the file if there was any
        fs.writeFile(outputFilePath, '', (err) => {
            if(err) {
                console.error(err);
            }
        });
    }

    readLine(callback) {

        this.lineReader.on('line', (line) => {
            return callback(null, line);
        });

    }

    writeLine(line, callback) {

        // Output line termination
        var termLine = line + '\n';

        fs.appendFile(this.outputFile, termLine, (err) => {
            if(err) {
                return callback(err);
            }
        });
    }
}

module.exports = IO;
