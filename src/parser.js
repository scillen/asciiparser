/**
 * Created by ellen on 10/04/16.
 */

"use strict";

const _ = require('lodash');

const OptionsDictionary = [
    {
        '   ': ['1', '4', '?'],
        ' _ ': ['0', '2', '3', '5', '6', '7', '8', '9', '?']
    },
    {
        '  |': ['1', '7', '?'],
        '| |': ['0', '?'],
        '|_ ': ['5', '6', '?'],
        ' _|': ['2', '3', '?'],
        '|_|': ['4', '8', '9', '?']
    },
    {
        '  |': ['1', '4', '7', '?'],
        '|_ ': ['2', '?'],
        ' _|': ['3', '5', '9', '?'],
        '|_|': ['0', '6', '8', '?']
    }
];

class Parser {
    constructor(config) {
        this.options = [];
        this.config = config;
        this.initOptions();
    }

    /*
    * initOptions
    *
    * Options arrays initializer.
    *
    * The arrays number is as number of digits in invoice number.
    * Each array consists initially from all possible digits 0-9 and the char of '?' (for error handling)
    * */
    initOptions() {
        const arrNum = this.config.invoiceNumberLength;
        for(var i=0; i<arrNum; i++) {
            this.options[i] = [];
            for(var j=0; j<10; j++) {
                this.options[i][j] = j.toString();
            }
            this.options[i][10] = '?';
        }
    }

    getOptions() {
        return this.options;
    }

    /*
    * filterByDictionary
    *
    * Options array filtering using a dictionary for eliminating the impossible for current input options.
     *
    * */
    filterByDictionary(row, rowInd) {

        if(row.length < 3*this.config.invoiceNumberLength) {
            //Fixing unfortunate trimming of a last white space by readline module.
            row += ' ';
        }

        for(var i=0, j=0; j < this.config.invoiceNumberLength; i+=3, j++) {

            var part = row[i]+row[i+1]+row[i+2];
            var currOptions = OptionsDictionary[rowInd][part];

            if(typeof currOptions === 'undefined' || !currOptions ) {
                // Illegal input
                currOptions = ['?'];
            }

            this.options[j] = _.intersection(this.options[j], currOptions);
        }
    }

    /*
    * parseRow
    *
    * Parse single row.
    *    middle digit row - filter the options
    *    last row - finalize the number
    *
    * */
    parseRow(row, rowInd, callback) {
        if(row === "") {
            this.finalizeNumber((err, number) => {
                if(err) {
                    console.error(err);
                    callback(err);
                }
                else {
                    callback(null, number);
                }
            })
        }
        else {
            this.filterByDictionary(row, rowInd);
        }
    }

    /*
    * finalizeNumber
    *
    * Builds output number and initializes options for next number
    *
    * */
    finalizeNumber(callback) {
        var result = "";
        var illegal = false;
        var error = "";

        for(var i=0; i<this.config.invoiceNumberLength; i++){

            if(this.options[i].length == 1) {
                // Illegal character
                illegal = true;
            }
            else if(this.options[i].length > 2) {
                error += "Unable to determine the digit "+ i +" - too many options left\n";

                // The number will be corrupted and there is no reason to continue
                // Init the options for the next number
                this.initOptions(this.config.invoiceNumberLength);
                return callback(error);
            }

            result += this.options[i][0];
        }

        if(illegal) {
            result += ' ILLEGAL';
        }

        //console.log(result);

        // Init the options for the next number
        this.initOptions(this.config.invoiceNumberLength);

        return callback(null, result);
    }
}

module.exports = Parser;
module.exports.OptionsDictionary = OptionsDictionary;