/**
 * Created by ellen on 17/04/16.
 */

"use strict";

const expect = require('chai').expect;
const assert = require('chai').assert;
const _      = require('lodash');


const Parser = require('../src/parser');
const OptionsDictionary = require('../src/parser').OptionsDictionary;

describe('Parser Init TESTS', () => {
    //Init function tests
    for(var i = 1; i < 10; i++) {
        initTests(i);
    }
});

describe('Digits parsing TESTS', () => {
    var digits = [
        [' _ ', '| |', '|_|'], //0
        ['   ', '  |', '  |'], //1
        [' _ ', ' _|', '|_ '], //2
        [' _ ', ' _|', ' _|'], //3
        ['   ', '|_|', '  |'], //4
        [' _ ', '|_ ', ' _|'], //5
        [' _ ', '|_ ', '|_|'], //6
        [' _ ', '  |', '  |'], //7
        [' _ ', '|_|', '|_|'], //8
        [' _ ', '|_|', ' _|'], //9
    ];

    _.forEach (digits, (digit, index) => {
        digitTests(digit, index.toString());
    });

});

describe('Errors in input TESTS: one error per digit by row', () => {

    var digitsWithErrorPerRow = [
        [ // Errors in row 0
            ['|_  ', '| |', '|_|'], //0
            ['|  ', '  |', '  |'], //1
            ['|_ ', ' _|', '|_ '], //2
            ['   ', ' _|', ' _|'], //3
            ['  |', '|_|', '  |'], //4
            ['   ', '|_ ', ' _|'], //5
            ['|  ', '|_ ', '|_|'], //6
            ['   ', '  |', '  |'], //7
            [' _|', '|_|', '|_|'], //8
            ['   ', '|_|', ' _|'], //9
        ],
        [ // Errors in row 1
            [' _ ', '|_|', '|_|'], //0
            ['   ', ' _|', '  |'], //1
            [' _ ', '  |', '|_ '], //2
            [' _ ', '  |', ' _|'], //3
            ['   ', '| |', '  |'], //4
            [' _ ', ' _ ', ' _|'], //5
            [' _ ', ' _ ', '|_|'], //6
            [' _ ', '| |', '  |'], //7
            [' _ ', ' _|', '|_|'], //8
            [' _ ', '|_  ', ' _|'], //9
        ],
        [ // Errors in row 2
            [' _ ', '| |', '|_ '], //0
            ['   ', '  |', '   '], //1
            [' _ ', ' _|', '|  '], //2
            [' _ ', ' _|', '  |'], //3
            ['   ', '|_|', ' _|'], //4
            [' _ ', '|_ ', '  |'], //5
            [' _ ', '|_ ', '| |'], //6
            [' _ ', '  |', ' _|'], //7
            [' _ ', '|_|', ' _|'], //8
            [' _ ', '|_| ', '|_|'], //9
        ]
    ];

    _.forEach(digitsWithErrorPerRow, (errorRow, rowIndex) => {
        _.forEach(errorRow, (digit, index) => {
            digitErrorsTests(digit, index.toString(), rowIndex);
        });
    })
});

describe('Errors in input TESTS: multiple errors per digit', () => {

    var digitsWithErrors = [
        ['|_  ', '| ', ' _|'], //0
        ['|_ ', '  |', '   '], //1
        ['|_ ', ' _|', '|_ '], //2
        ['   ', ' _|', ' _|'], //3
        ['  |', '|_|', '  |'], //4
        ['   ', '|_ ', ' _|'], //5
        ['|  ', '|_ ', '|_|'], //6
        ['   ', '  |', '  |'], //7
        [' _|', '|_|', '|_|'], //8
        ['   ', '|_|', ' _|'], //9
    ];

    _.forEach(digitsWithErrors, (digit, index) => {
        digitMultipleErrorsTests(digit, index.toString());
    });

});

function initTests(len) {
    describe('#initOptions with length '+len, () => {

        var parser = new Parser({invoiceNumberLength: len});

        var options = parser.getOptions();
        it('should create an array', () => {
            expect(options).to.be.an('array');
        });
        it('should create an array with length of '+len , () => {

            expect(options).to.have.length(len);
        });
        it('should create an array with all the possible chars', () => {
            const charsArray = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '?'];
            var array = _.fill(Array(len), charsArray);
            expect(options).to.deep.equal(array);
        });
    });
}


function digitTests(digit, exp) {
    describe('Parsing digit '+ exp, () => {
        var parser = new Parser({invoiceNumberLength: 1});

        //For each line in digit do the filtering
        _.forEach(digit, (value, index) => {
            filterRows(parser, index, value, exp);
        });

        // Finalize number
        finalizeDigit(parser, exp);
    });
}

function filterRows(parser, rowInd, char, exp) {
    describe('#filterByDictionary', () => {
        it('options after filtering line '+ rowInd + ' with "'+ char +'"' + ' should include result '+ exp , () => {
            parser.filterByDictionary(char, rowInd);

            var options = parser.getOptions()[0];
            assert.include(options, exp);
            expect(options).to.have.length.above(1);
        });
    });
}

function finalizeDigit(parser, exp) {
    describe('#finalizeNumber', () => {
        it('number should be '+ exp, () => {
            parser.finalizeNumber((err, number) => {
                expect(err).to.be.a('null');
                expect(number).to.be.equal(exp);
            })
        })
    })
}

function digitErrorsTests(digit, exp, errorInd) {
    describe('Parsing digit with error '+ exp, () => {
        var parser = new Parser({invoiceNumberLength: 1});

        //For each line in digit do the filtering
        _.forEach(digit, (value, index) => {
            filterErrorRows(parser, index, value, exp, errorInd);
        });

        // Finalize number
        finalizeErrorInDigit(parser, exp);
    });
}

function filterErrorRows(parser, rowInd, char, exp, errorInd) {
    describe('#filterByDictionary - ILLEGAL input', () => {
        if(rowInd >= errorInd) {
            it('options after filtering line ' + rowInd + ' with "' + char + '"' + ' should not include result ' + exp, () => {
                parser.filterByDictionary(char, rowInd);

                var options = parser.getOptions()[0];
                assert.include(options, '?');
                assert.notInclude(options, exp);
            });
        }
        else {
            it('options after filtering line '+ rowInd + ' with "'+ char +'"' + ' should include result '+ exp , () => {
                parser.filterByDictionary(char, rowInd);

                var options = parser.getOptions()[0];
                assert.include(options, exp);
                expect(options).to.have.length.above(1);
            });
        }
    });
}

function finalizeErrorInDigit(parser, exp) {
    describe('#finalizeNumber - ILLEGAL input', () => {
        it('number should be '+ exp, () => {
            parser.finalizeNumber((err, number) => {
                expect(err).to.be.a('null');
                expect(number).to.not.be.equal(exp);
            })
        })
    })
}

function digitMultipleErrorsTests(digit, exp) {
    describe('Parsing digit with error '+ exp, () => {
        var parser = new Parser({invoiceNumberLength: 1});

        //For each line in digit do the filtering
        _.forEach(digit, (value, index) => {
            parser.filterByDictionary(value, index);
        });

        // Finalize number
        finalizeErrorInDigit(parser, exp);
    });
}

